doh-stub is a very basic stub resolver written in perl

It's not supposed to be production ready or anything, and is just an exercise for me in playing with handling of dns over https via POST of wire format DNS

There's no config, so the endpoint is hard coded to cloudflare

There's a *really* crude cache